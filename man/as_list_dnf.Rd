% Generated by roxygen2: do not edit by hand
% Please edit documentation in R/dnf.R
\name{as_list_dnf}
\alias{as_list_dnf}
\title{Convert a DNF matrix to a DNF list}
\usage{
as_list_dnf(dnf)
}
\arguments{
\item{dnf}{A DNF}
}
\description{
This function returns a matrix representation of a DNF.
}

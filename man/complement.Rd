% Generated by roxygen2: do not edit by hand
% Please edit documentation in R/sets.R
\name{complement}
\alias{complement}
\title{Complement of a set}
\usage{
complement(x)
}
\arguments{
\item{x}{A set}
}
\description{
Complement of a set
}
